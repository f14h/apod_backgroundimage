#! /bin/bash

function urldecode()
{
	: "${*//+/ }"; 
	echo -e "${_//%/\\x}";
}

SCREEN_W=5120;
SCREEN_H=1440;

apod_url="https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY";
page=$(wget -qO - "$apod_url");

ex=$(echo "$page" | jq -r '.explanation' | sed 's/  / /g' | sed 's/  /\n/g');
url=$(echo "$page" | jq -r '.hdurl');
title=$(echo "$page" | jq -r '.title');
file_t=$(urldecode `echo "${url##*/}" | cut -f1 -d"?"`);

wget -q "$url" -O "/tmp/$file_t";

#cp ~/DATA/background/2021-03-31_M87bhPolarized_Eht_3414.jpg "/tmp/$file_t"
convert "/tmp/$file_t" -resize "$SCREEN_W"x"$SCREEN_H"^ -gravity center -extent "$SCREEN_W"x"$SCREEN_H" /tmp/background_orig_res.png || exit 1;

convert -bordercolor '#00000080' -border 2x2 -background '#00000080' -pointsize 19 -fill '#ffffff80' label:"$title" miff:- | composite -gravity north-west -geometry +5+30 - /tmp/background_orig_res.png /tmp/background_tmp.png || mv tmp/background_orig_res.png /tmp/background_tmp.png;

convert -size 700x -bordercolor '#00000080' -border 2x2 -background '#00000080' -pointsize 15 -fill '#ffffff80' caption:"$ex" miff:- | composite -gravity south-west -geometry +10+5 - /tmp/background_tmp.png /tmp/background.png || mv /tmp/background_tmp.png /tmp/background.png;

rm -f /tmp/background_tmp.png;
rm -f /tmp/background_orig_res.png;

mv /tmp/background.png ~/Bilder/background.png;
mv "/tmp/$file_t" ~/DATA/background/$(date +'%F')_"$file_t";

echo "$title" > ~/DATA/background/$(date +'%F').txt;
echo "==========" >> ~/DATA/background/$(date +'%F').txt;
echo "$ex" >> ~/DATA/background/$(date +'%F').txt;

ln -sf ~/DATA/background/$(date +'%F')_"$file_t" "/home/frederik/.zoom/data/VirtualBkgnd_Custom/{821739ac-548a-455e-8631-b7af3cf4744b}"
